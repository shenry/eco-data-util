An opinionated way to make a python project.

## Suggested Reading ##

[Starting an Open Source Project](https://opensource.guide/starting-a-project/)

We are going to use the Pipfile so we can create a reproducible env for "production"
