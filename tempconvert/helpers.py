SUPPORTED_UNITS = ["K", "C", "F"]


def unpack_list(s):
    return ",".join(map(str, s))


class UnitNotSupported(Exception):

    def __init__(self, unit_in, unit_out):
        Exception.__init__(
            self,
            f"Can't convert {unit_in} to {unit_out}. Only {unpack_list(SUPPORTED_UNITS)} are supported",
        )
