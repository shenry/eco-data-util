from tempconvert.helpers import SUPPORTED_UNITS, UnitNotSupported


class Convert:
    """
    Convert temperature from specified unit to another specified unit.

    We should see if we can inject SUPPORTED_UNITS into a doc string!

    Parameters
    ----------
    value : float
        Temperature value to be converted
    unit : str
        Unit of temperature value to be converted
    out_unit : str `K`, optional
        Unit of temperature output, defaults to Kelvin

    Returns
    -------
    float
        Value of converted temperature

    Raises
    ------
    UnitNotSupported
        If `value` or `out_unit` are not in SUPPORTED_UNITS

    """

    def __init__(self, value, unit, out_unit="K"):
        self.value = value
        self.unit = unit.upper()
        self.out_unit = out_unit.upper()

        if self.unit not in SUPPORTED_UNITS or self.out_unit not in SUPPORTED_UNITS:
            raise UnitNotSupported(self.unit, self.out_unit)

        def convert_input_to_kelvin(self):
            raise NotImplementedError

        def convert_kelvin_to_output(self):
            raise NotImplementedError
